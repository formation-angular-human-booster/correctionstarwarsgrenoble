import { Component, OnInit } from '@angular/core';
import {Planet} from '../../models/planet';
import {PlanetService} from '../../services/planet.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-planet-detail',
  templateUrl: './planet-detail.component.html',
  styleUrls: ['./planet-detail.component.css']
})
export class PlanetDetailComponent implements OnInit {
  planet: Planet;
  isLoading: boolean;
  constructor(private planetService: PlanetService,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    const id = +this.activatedRoute.snapshot.paramMap.get('id');
    this.isLoading = true;
    this.planetService.getPlanetById(id).subscribe((data: Planet) => {
      this.planet = data;
      this.isLoading = false;
    });
  }

}
