import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Planet} from '../../models/planet';
import {PlanetService} from '../../services/planet.service';
import loader from '@angular-devkit/build-angular/src/angular-cli-files/plugins/single-test-transform';

@Component({
  selector: 'app-edit-planet',
  templateUrl: './edit-planet.component.html',
  styleUrls: ['./edit-planet.component.css']
})
export class EditPlanetComponent implements OnInit {
  planetToUpdate: Planet;
  yesNoItem = [{text: 'Oui', valeur: true}, {text: 'Non', valeur: false}];
  loader: boolean;
  constructor(private activatedRoute: ActivatedRoute,
              private planetService: PlanetService,
              private router: Router) { }

  ngOnInit() {
    const id = +this.activatedRoute.snapshot.paramMap.get('id');
    this.loader = true;
    this.planetService.getPlanetById(id).subscribe((data: Planet) => {
      this.planetToUpdate = data;
      this.loader = false;
    });
  }

  updatePlanet() {
    this.loader = true;
    this.planetService.updatePlanet(this.planetToUpdate).subscribe((data: Planet) => {
      this.router.navigate(['/planets']);
      this.loader = false;
    });
  }
}
