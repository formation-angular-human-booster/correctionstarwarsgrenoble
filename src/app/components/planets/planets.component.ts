import { Component, OnInit } from '@angular/core';
import {PlanetService} from '../../services/planet.service';
import {Planet} from '../../models/planet';
import {Router} from '@angular/router';

@Component({
  selector: 'app-planets',
  templateUrl: './planets.component.html',
  styleUrls: ['./planets.component.css']
})
export class PlanetsComponent implements OnInit {
  planets: Planet[];
  isLoading: boolean;
  constructor(private planetService: PlanetService) { }

  ngOnInit() {
    this.isLoading = true;
    this.planetService.getPlanets().subscribe((data: Planet[]) => {
      this.planets = data;
      this.isLoading = false;
    });

  }

  deletePlanet(planet: Planet) {
    this.isLoading = true;
    this.planetService.delete(planet).subscribe((data: Planet) => {
      this.planetService.getPlanets().subscribe((allPlanet: Planet[]) => {
        this.planets = allPlanet;
        this.isLoading = false;
      });
    });
  }

}
