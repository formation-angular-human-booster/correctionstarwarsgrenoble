import { Injectable } from '@angular/core';
import {Planet} from '../models/planet';
import {HttpClient} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, retry} from 'rxjs/internal/operators';

@Injectable({
  providedIn: 'root'
})
export class PlanetService {
  apiUrl = 'http://localhost:3000/planet';
  planets: Planet[];

  constructor(private httpClient: HttpClient) {
  }

  getPlanets(): Observable<Planet[]> {
    return this.httpClient.get<Planet[]>(this.apiUrl).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  getPlanetById(id: number): Observable<Planet> {
    return this.httpClient.get<Planet>(this.apiUrl + '/' + id).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }


  handleError(error) {
    let errorMessage = '';
    if ( error.error instanceof ErrorEvent ) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  addPlanet(planet: Planet): void {
    this.planets.push(planet);
  }

  delete(planetToDelete: Planet): Observable<Planet> {
    return this.httpClient.delete<Planet>(this.apiUrl + '/' + planetToDelete.id).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  updatePlanet(planetToUpdate: Planet): Observable<Planet> {
    return this.httpClient.put<Planet>(this.apiUrl + '/' + planetToUpdate.id, planetToUpdate);
  }
}
