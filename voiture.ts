export class Cars {
  id: number;
  marque: string;
  model: string;
  moteur: string;
  gasoil: string;
  gear: string;
  nbPlace: number;
  cameraRecul: boolean;
  jantesAlliage: boolean;
  gps: boolean;
  nbKm: number;
  mec: string;
  img: string;
  nbrporte: number;
  couleur: string;
  prixVente: number;
  mel: Date;


}
